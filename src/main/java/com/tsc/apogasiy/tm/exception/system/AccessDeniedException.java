package com.tsc.apogasiy.tm.exception.system;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied!");
    }
}
