package com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    void add(@NotNull final User user);

    @NotNull
    User create(@Nullable final String login, @Nullable final String password);

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @NotNull final String email);

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @NotNull final Role role);

    @NotNull
    User setPassword(@Nullable final String userId, @Nullable final String password);

    @NotNull
    User setRole(@Nullable final String userId, @Nullable final Role role);

    void remove(@NotNull final User user);

    void clear();

    @Nullable
    User findById(@Nullable final String id);

    @Nullable
    User findByLogin(@Nullable final String login);

    @Nullable
    User findByEmail(@Nullable final String email);

    @Nullable
    User removeById(@Nullable final String id);

    @Nullable
    User removeByLogin(@Nullable final String login);

    boolean isLoginExists(@NotNull final String login);

    boolean isEmailExists(@NotNull final String email);

    @NotNull
    User updateById(@Nullable final String id, @Nullable final String lastName, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String email);

    @NotNull
    User updateByLogin(@Nullable final String login, @Nullable final String lastName, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String email);

    @NotNull
    User lockUserByLogin(@Nullable final String login);

    @NotNull
    User unlockUserByLogin(@Nullable final String login);

}
