package com.tsc.apogasiy.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @Nullable ITaskService getTaskService();

    @Nullable IProjectService getProjectService();

    @Nullable IProjectTaskService getProjectTaskService();

    @Nullable ICommandService getCommandService();

    @Nullable IUserService getUserService();

    @Nullable IAuthService getAuthService();

}
