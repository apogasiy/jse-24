package com.tsc.apogasiy.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    @Nullable String getCurrentUserId();

    void setCurrentUserId(@NotNull final String currentUserId);

}
