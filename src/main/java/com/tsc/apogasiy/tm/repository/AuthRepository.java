package com.tsc.apogasiy.tm.repository;

import com.tsc.apogasiy.tm.api.repository.IAuthRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthRepository implements IAuthRepository {

    @Nullable private String currentUserId;

    @Override
    @Nullable public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(@NotNull final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
