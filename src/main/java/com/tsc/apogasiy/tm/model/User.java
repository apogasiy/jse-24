package com.tsc.apogasiy.tm.model;

import lombok.Getter;
import lombok.Setter;
import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class User extends AbstractEntity {

    @Getter @Setter @NotNull private String login;
    @Getter @Setter @NotNull private String password;
    @Getter @Setter @Nullable private String email;
    @Getter @Setter @Nullable private String lastName = "";
    @Getter @Setter @Nullable private String firstName = "";
    @Getter @Setter @Nullable private String middleName = "";
    @Getter @Setter @Nullable private Role role = Role.USER;
    @Getter @Setter @NotNull private Boolean locked = false;

    public User(@NotNull final String login, @NotNull String password) {
        this.login = login;
        this.password = HashUtil.encrypt(password);
    }

}
