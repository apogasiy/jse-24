package com.tsc.apogasiy.tm.service;

import lombok.RequiredArgsConstructor;
import com.tsc.apogasiy.tm.api.repository.ICommandRepository;
import com.tsc.apogasiy.tm.api.service.ICommandService;
import com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor
public class CommandService implements ICommandService {

    @Nullable private final ICommandRepository commandRepository;

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getArgNames();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

}
